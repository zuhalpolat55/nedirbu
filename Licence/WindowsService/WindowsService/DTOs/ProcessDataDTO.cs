﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsService.DTOs
{
    class ProcessDataDTO
    {
        public ProcessDataDTO(string ipAddress, string machineName, string name, float cpu, float ram, float disk)
        {
            IpAddress = ipAddress;
            MachineName = machineName;
            Name = name;
            Cpu = cpu;
            Ram = ram;
            Disk = disk;
            PTimestamp = DateTime.Now;
        }

        public string IpAddress { get; set; }
        public string MachineName { get; set; }
        public string Name { get; set; }
        public float Cpu { get; set; }
        public float Ram { get; set; }
        public float Disk { get; set; }
        public DateTime PTimestamp { get; set; }
    }
}
