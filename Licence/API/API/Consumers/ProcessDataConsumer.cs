using System.Threading.Tasks;
using MassTransit;
using API.DTOs;
using API.Data;
using AutoMapper;
using API.Repositories;
using API.Models;

namespace API.Consumers
{
    public class ProcessDataConsumer : IConsumer<IProcessDataDTO>
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IProcessDataRepository _processDataRepository;

        public ProcessDataConsumer(DataContext context, IMapper mapper, IProcessDataRepository processDataRepository)
        {
            _context = context;
            _mapper = mapper;
            _processDataRepository = processDataRepository;
        }
        
        public async Task Consume(ConsumeContext<IProcessDataDTO> context)
        {
            IProcessDataDTO processDataDTO = context.Message;
            var process = _mapper.Map<ProcessData>(context.Message);
            _context.ProcessData.Add(process);

            var result = await _context.SaveChangesAsync();
        }
    }
}