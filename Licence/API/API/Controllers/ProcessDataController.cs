﻿using API.Data;
using API.DTOs;
using API.Models;
using API.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]

    public class ProcessDataController : Controller
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IProcessDataRepository _processDataRepository;

        public ProcessDataController(DataContext context, IMapper mapper, IProcessDataRepository processDataRepository)
        {
            _context = context;
            _mapper = mapper;
            _processDataRepository = processDataRepository;
        }

        [HttpPost]
        public async Task<IActionResult> PostProcess([FromBody] ProcessDataDTO processDto)
        {
            var bus = BusConfigurator.ConfigureBus();

            var sendToUri = new Uri($"{RabbitMqConsts.RabbitMqUri}{RabbitMqConsts.ProcessDomainService}");
            var endPoint = await bus.GetSendEndpoint(sendToUri);

            await endPoint.Send<IProcessDataDTO>(processDto);
            
            return Ok();
        }

        [HttpGet()]
        public async Task<ActionResult<IEnumerable<ProcessDataDTO>>> GetProcess(int minutes, string ipAddress)
        {
            if (minutes < 0)
                return BadRequest();

            var process = await _processDataRepository.GetProcessUsageByTime(minutes, ipAddress);

            return Ok(process);
        }


        [HttpGet("get-report")]
        public async Task<ActionResult<ProcessReportDTO>> GetProcessReport(int minutes, string ipAddress, string[] labels)
        {
            if (minutes < 0)
                return BadRequest();

            var result = await _processDataRepository.GetProcessUsageReport(minutes, ipAddress, labels);
            return Ok(result);
        }


        [HttpGet("get-users")]
        public async Task<IEnumerable<string>> GetUsers()
        {
            return await _processDataRepository.GetUsers();
        }
    }
}
