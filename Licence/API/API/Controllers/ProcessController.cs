﻿using API.Data;
using API.DTOs;
using API.Models;
using API.Repositories;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class ProcessController : Controller
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        private readonly IProcessRepository _processRepository;

        public ProcessController(DataContext context, IMapper mapper, IProcessRepository processRepository)
        {
            _context = context;
            _mapper = mapper;
            _processRepository = processRepository;
        }

        [HttpPost]
        public async Task<IActionResult> PostProcess([FromBody] ProcessDTO processDto)
        {
            if (processDto == null)
                return BadRequest();

            var process = _mapper.Map<Processes>(processDto);
            _context.Processes.Add(process);
            var result = await _context.SaveChangesAsync();

            return Ok();

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Processes>>> GetProcessList()
        {
            var process = await _processRepository.GetProcessList();

            return Ok(process);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProcessById(int id)
        {
            var process = _processRepository.GetProcessById(id);

            if (process != null)
            {
                _processRepository.DeleteProcess(process);
                return Ok();
            }

            return NotFound();
        }
    }
}
