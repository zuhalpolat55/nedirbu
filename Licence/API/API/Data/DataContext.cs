﻿using API.Models;
using Microsoft.EntityFrameworkCore;


namespace API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Processes> Processes { get; set; }
        public DbSet<ProcessData> ProcessData { get; set; }
    }
}
