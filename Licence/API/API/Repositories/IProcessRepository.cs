﻿using API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories
{
    public interface IProcessRepository
    {
        Task<IEnumerable<Processes>> GetProcessList();
        void DeleteProcess(Processes process);
        Processes GetProcessById(int id);
    }
}
