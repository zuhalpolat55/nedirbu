﻿using API.Data;
using API.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories
{
    public class ProcessRepository : IProcessRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public ProcessRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void DeleteProcess(Processes process)
        {
            _context.Processes.Remove(process);
            _context.SaveChanges();
        }

        public Processes GetProcessById(int id)
        {
            return _context.Processes
                .Where(a => a.Id == id).FirstOrDefault();
        }

        public async Task<IEnumerable<Processes>> GetProcessList()
        {
            return await _context.Processes.ToListAsync();

        }
    }
}
