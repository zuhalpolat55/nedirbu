﻿using API.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories
{
    public interface IProcessDataRepository
    {
        Task<IEnumerable<ProcessDataDTO>> GetProcessUsageByTime(int minutes, string ipAddress);
        Task<IEnumerable<ProcessReportDTO>> GetProcessUsageReport(int minutesi, string ipAddress, string[] labels);
        Task<IEnumerable<string>> GetUsers();
    }
}
