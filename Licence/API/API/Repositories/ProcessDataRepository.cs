﻿using API.Data;
using API.DTOs;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Repositories
{
    public class ProcessDataRepository : IProcessDataRepository
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public ProcessDataRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProcessDataDTO>> GetProcessUsageByTime(int minutes, string ipAddress)
        {
            var response = await _context.ProcessData
                .Where(x => x.PTimestamp > DateTime.Now.AddMinutes(-minutes) && x.IpAddress == ipAddress)
                .ToListAsync();

            return _mapper.Map<IEnumerable<ProcessDataDTO>>(response);
        }

        public async Task<IEnumerable<ProcessReportDTO>> GetProcessUsageReport(int minutes, string ipAddress, string[] labels)
        {
            List<ProcessReportDTO> reports = new List<ProcessReportDTO>();
            foreach (string label in labels)
            {
                var response = await _context.ProcessData
                    .Where(a => a.Cpu > 0.000 && a.PTimestamp > DateTime.Now.AddMinutes(-minutes) && a.IpAddress == ipAddress && a.Name == label).ToListAsync();

                if (response.Count != 0)
                {

                    ProcessReportDTO cpu = new ProcessReportDTO(label, "Cpu", response.Max(a => a.Cpu), response.Min(a => a.Cpu), response.Average(a => a.Cpu));
                    ProcessReportDTO ram = new ProcessReportDTO(label, "Ram", response.Max(a => a.Ram), response.Min(a => a.Ram), response.Average(a => a.Ram));
                    ProcessReportDTO disk = new ProcessReportDTO(label, "Disk", response.Max(a => a.Disk), response.Min(a => a.Disk), response.Average(a => a.Disk));

                    reports.Add(cpu);
                    reports.Add(ram);
                    reports.Add(disk);
                }
            }
            return reports;
        }

        public async Task<IEnumerable<string>> GetUsers()
        {
            var response = await _context.ProcessData
                .Select(x => x.IpAddress)
                .Distinct()
                .ToListAsync();

            return response;
        }
    }
}
