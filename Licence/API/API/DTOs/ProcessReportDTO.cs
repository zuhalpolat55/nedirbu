﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.DTOs
{
    public class ProcessReportDTO
    {
        public ProcessReportDTO()
        {
        }

        public ProcessReportDTO(string processName, string name, float max, float min, float avg)
        {
            ProcessName = processName;
            Name = name;
            Max = max;
            Min = min;
            Avg = avg;
        }
        public string ProcessName { get; set; }
        public string Name { get; set; }
        public float Max { get; set; }
        public float Min { get; set; }
        public float Avg { get; set; }
    }
}
