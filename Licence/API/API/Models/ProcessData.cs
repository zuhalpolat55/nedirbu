﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class ProcessData
    {
        public int Id { get; set; }
        public string IpAddress { get; set; }
        public string MachineName { get; set; }
        public string Name { get; set; }
        public float Cpu { get; set; }
        public float Ram { get; set; }
        public float Disk { get; set; }
        public DateTime PTimestamp { get; set; }
    }
}
