Merhaba ben Zuhal Polat, Eskişehir Osmangazi Üniversitesi Bilgisayar Mühendisliği bölümü öğrencisiyim.

TEI'de Bilişim Teknolojileri bölümünde Yazılım Ekibinde uzun dönem staj yapmaktayım.

Savunma sanayi merak ettiğim bir sektördü ve bu alanda faaliyet veren bir firmada staj yapmak istiyordum.
Savunma sanayinin önde gelen şirketlerinden biri olan TEI'de staj yapma şansım oldu ve bu süreçte şirket içi projelerde 
sorumluluk ve aktif rol alarak kendimi geliştirme ve profesyonel iş hayatına hazırlanma fırsatı buldum. 
Dünyada alanında kadın çalışanlara en çok önem veren firma olan TEI'de bir kadın mühendis adayı olarak çalışma fırsatı bulmak 
ve deneyimlemek ayrıca mutluluk vericiydi.

 

